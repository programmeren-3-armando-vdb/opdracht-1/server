package be.kdg.examen.gedistribueerde;

import be.kdg.examen.gedistribueerde.server.ServerSkel;

public class StartNonDistributedServer {
    public static void main(String[] args) {
        ServerSkel serverSkel = new ServerSkel();
        serverSkel.run();
    }
}
