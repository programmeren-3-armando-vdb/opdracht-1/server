package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.client.DocumentImpl;
import be.kdg.examen.gedistribueerde.communications.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communications.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communications.communication.NetworkAddress;

public class ServerSkel {
    private final Server server;
    private final MessageManager messageManager;

    public ServerSkel() {
        messageManager = new MessageManager();
        System.out.println("My IP address = " + messageManager.getMyAddress());
        this.server = new ServerImpl();
    }

    private void handleRequest(MethodCallMessage request) {
        String methodName = request.getMethodName();
        if ("log".equals(methodName)) {
            handleLog(request);
        } else if ("toUpper".equals(methodName)) {
            handleToUpper(request);
        } else if ("toLower".equals(methodName)) {
            handleToLower(request);
        } else if ("type".equals(methodName)) {
            handleType(request);
        } else if ("create".equals(methodName)) {
            handleCreate(request);
        } else {
            System.out.println("ServerSkeleton recieved an unknown request: " + methodName);
        }
    }

    private void handleCreate(MethodCallMessage request) {
        Document doc = server.create(request.getParameter("s"));
        MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
        response.setParameter("docText", doc.getText());
        messageManager.send(response, request.getOriginator());
    }

    private void handleType(MethodCallMessage request) {
        String text = request.getParameter("docText");
        NetworkAddress docSkel = new NetworkAddress(request.getParameter("ip"),Integer.parseInt(request.getParameter("port")));
        DocStub docStub = new DocStub(docSkel);
        server.type(docStub, text);
        MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
        messageManager.send(response, request.getOriginator());
    }

    private void handleToLower(MethodCallMessage request) {
        handleTextManipulation(request, "handleToLower");
    }

    private void handleToUpper(MethodCallMessage request) {
        handleTextManipulation(request, "handleToUpper");
    }

    private void handleTextManipulation(MethodCallMessage request, String methodName) {
        String text = request.getParameter("docText");
        MethodCallMessage response = new MethodCallMessage(messageManager.getMyAddress(), "result");
        if ("handleToUpper".equals(methodName)) {
            response.setParameter("docText", text.toUpperCase());
        } else if ("handleToLower".equals(methodName)) {
            response.setParameter("docText", text.toLowerCase());
        }
        messageManager.send(response, request.getOriginator());
    }

    private void handleLog(MethodCallMessage request) {
        Document document = new DocumentImpl(request.getParameter("text"));
        server.log(document);
        sendEmptyReply(request);
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    public void run() {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            handleRequest(request);
        }
    }
}
