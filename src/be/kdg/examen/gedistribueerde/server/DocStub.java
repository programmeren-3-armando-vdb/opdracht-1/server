package be.kdg.examen.gedistribueerde.server;

import be.kdg.examen.gedistribueerde.client.Document;
import be.kdg.examen.gedistribueerde.communications.communication.MessageManager;
import be.kdg.examen.gedistribueerde.communications.communication.MethodCallMessage;
import be.kdg.examen.gedistribueerde.communications.communication.NetworkAddress;

public class DocStub implements Document {
    private final NetworkAddress networkAddress;
    private final MessageManager messageManager;

    DocStub(NetworkAddress networkAddress) {
        this.networkAddress = networkAddress;
        this.messageManager = new MessageManager();
    }

    @Override
    public String getText() {
        return null;
    }

    @Override
    public void setText(String text) {

    }

    @Override
    public void append(char c) {
        MethodCallMessage message = new MethodCallMessage(messageManager.getMyAddress(),"append");
        message.setParameter("char",String.valueOf(c));
        messageManager.send(message,networkAddress);
        checkEmptyReply();
    }

    @Override
    public void setChar(int position, char c) {

    }

    private void checkEmptyReply() {
        String value = "";
        while (!"Ok".equals(value)){
            MethodCallMessage reply = messageManager.wReceive();
            if (!"result".equals(reply.getMethodName())){
                continue;
            }
            value = reply.getParameter("result");
        }
        System.out.println("Received OK reply.");
    }
}
